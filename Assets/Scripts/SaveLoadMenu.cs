﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;

public class SaveLoadMenu : MonoBehaviour {

	public HexGrid hexGrid;

	public Text menuLabel, actionButtonLabel;
	public InputField nameInput;

	public RectTransform listContent;
	public SaveLoadItem itemPrefab;

	bool saveMode;

	public void Open (bool saveMode) {
		this.saveMode = saveMode;

		if (saveMode) {
			menuLabel.text = "Save Map";
			actionButtonLabel.text = "Save";
		} else {
			menuLabel.text = "Load Map";
			actionButtonLabel.text = "Load";			
		}

		FillList();

		gameObject.SetActive(true);
		HexMapCamera.Locked = true;
	}

	public void Close () {
		gameObject.SetActive(false);
		HexMapCamera.Locked = false;
	}

	string GetSelectedPath () {
		string mapName = nameInput.text;
		if (mapName.Length == 0) {
			return null;
		}
		return Path.Combine(Application.persistentDataPath, mapName + ".map");
	}

	public void Action () {
		string path = GetSelectedPath();
		if (path == null) {
			return;
		}
		if (saveMode) {
			Save(path);
		} else {
			Load(path);
		}
		Close();
	}

	public void Save (string path) {
		Debug.Log("Save file path" + path);
		using ( // When writer is inside using block, there is no need to manually close the stream
			BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create))
		) {
			writer.Write(0); // Write 0 as savefile wersion number
			hexGrid.Save(writer);
		}
	}

	public void Load (string path) {
		// Check if file exists
		if (!File.Exists(path)) {
			Debug.LogError("File does not exist " + path);
			return;
		}

		using (
			BinaryReader reader = new BinaryReader(File.OpenRead(path))
		) {
			int version = reader.ReadInt32(); // Read savefile version number
			if (version == 0) {
				hexGrid.Load(reader, version);
				HexMapCamera.ValidatePosition(); // Reset camera  position
			} else {
				Debug.LogWarning("Unknown map format " + version);
			}
		}
	}

	public void SelectItem (string name) {
		nameInput.text = name;
	}

	public void Delete () {
		string path = GetSelectedPath();
		if (path == null) {
			return;
		}

		if(File.Exists(path)) {
			File.Delete(path);
		}
		nameInput.text = "";
		FillList();
	}

	void FillList () {
		// Empty list first
		for (int i = 0; i < listContent.childCount; i++) {
			Destroy(listContent.GetChild(i).gameObject);
		}

		string[] paths = Directory.GetFiles(Application.persistentDataPath, "*.map");
		Array.Sort(paths);

		for (int i = 0; i < paths.Length; i++) {
			SaveLoadItem item = Instantiate(itemPrefab);
			item.menu = this;
			item.MapName = Path.GetFileNameWithoutExtension(paths[i]);
			item.transform.SetParent(listContent, false);
		}
	}
}
