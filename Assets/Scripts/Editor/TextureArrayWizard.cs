﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TextureArrayWizard : ScriptableWizard {

	public Texture2D[] textures;

	[MenuItem("Assets/Create/Texture Array")] // Add new menu object to "Assets/Create". It will be linked to this function
	static void CreateWizard () {
		ScriptableWizard.DisplayWizard<TextureArrayWizard>("Create Texture Array", "Create");
	}

	void OnWizardCreate () { // This function is run when wizards create button is pressed
		if (textures == null || textures.Length == 0) {	
			return;
		}

		string path = EditorUtility.SaveFilePanelInProject("Save Texture Array", "Texture Array", "asset", "Save Texture Array");
		if (path.Length == 0) {
			return;
		}

		Texture2D t = textures[0];
		Texture2DArray textureArray = new Texture2DArray(
			t.width, t.height, textures.Length, t.format, t.mipmapCount > 1
		);
		textureArray.anisoLevel = t.anisoLevel;
		textureArray.filterMode = t.filterMode;
		textureArray.wrapMode = t.wrapMode;

		// Copy textures to textureArray
		for (int i = 0; i < textures.Length; i++) {
			for (int m = 0; m < t.mipmapCount; m++) {
				Graphics.CopyTexture(textures[i], 0, m, textureArray, i, m);
			}
		}

		// Create asset
		AssetDatabase.CreateAsset(textureArray, path);
	}
}
