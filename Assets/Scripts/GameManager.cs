﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	
	public int woodAmount = 0;
	public int woodGeneration = 1;
	public int rockAmount = 0;
	public int rockGeneration = 1;
	public int foodAmount = 0;
	public int foodGeneration = 1;
	public int scienceAmount = 0;
	public int scienceGeneration = 0;

	public Text woodLabel;
	public Text rockLabel;
	public Text foodLabel;
	public Text scienceLabel;

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject); // Set this to not be destroyed whene reloading scene

		InvokeRepeating("AddResources", 0f, 1f); // Run function every second
	}

	void Update () {

	}

	void AddResources () {
		woodAmount += woodGeneration / 60;
		rockAmount += rockGeneration / 60;
		foodAmount += foodGeneration / 60;
		scienceAmount += scienceGeneration / 60;

		UpdateLabels();
	}

	void UpdateLabels () {
		woodLabel.text = "Wood\n" + woodAmount + "\n+" + woodGeneration;
		rockLabel.text = "Rock\n" + rockAmount + "\n+" + rockGeneration;		
		foodLabel.text = "Food\n" + foodAmount + "\n+" + foodGeneration;
		scienceLabel.text = "Science\n" + scienceAmount + "\n+" + scienceGeneration;
	}
}
