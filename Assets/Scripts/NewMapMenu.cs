﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMapMenu : MonoBehaviour {

	public HexGrid hexGrid;

	public void Open () {
		gameObject.SetActive(true);
		HexMapCamera.Locked = true;
	}

	public void Close () {
		gameObject.SetActive(false);
		HexMapCamera.Locked = false;
	}

	void CreateMap(int x, int z) {
		hexGrid.CreateMap(x, z);
		HexMapCamera.ValidatePosition(); // reset camera position
		Close();
	}

	public void CreateSmallMap() {
		CreateMap(8, 6);
	}

	public void CreateMediumMap () {
		CreateMap(12, 9);
	}

	public void CreateLargeMap () {
		CreateMap(20, 15);
	}
}
