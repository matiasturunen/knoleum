﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class HexCell : MonoBehaviour {

	public HexCoordinates coordinates;
	public RectTransform uiRect;

	int elevation = int.MinValue; // Set elevation initially to something we will never use
	int waterLevel;
	int terrainTypeIndex;
	int level = 0;

	public HexGridChunk chunk;

	public HexPropManager propManager;

	[SerializeField]
	HexCell[] neighbors;

	public HexCell GetNeighbor (HexDirection direction) {
		return neighbors[(int)direction];
	}

	public void SetNeighbor (HexDirection direction, HexCell cell) {
		neighbors[(int) direction] = cell;
		cell.neighbors[(int)direction.Opposite()] = this; // Set cells neigbor be this cell
	}

	public int Elevation {
		get {
			return elevation;
		}
		set {
			if (elevation == value) {
				return; // Dont update elevation vhen it does not change
			}
			// On elevation change move hex and its labels according to set elevation
			elevation = value;
			RefreshPosition();

			Refresh();
		}
	}

	public HexEdgeType GetEdgeType (HexDirection direction) {
		return HexMetrics.GetEdgeType(elevation, neighbors[(int)direction].elevation);
	}

	public HexEdgeType GetEdgeType (HexCell otherCell) {
		return HexMetrics.GetEdgeType(elevation, otherCell.elevation);
	}

	public Vector3 Position {
		get {
			return transform.localPosition;
		}
	}

	void RefreshPosition() {
		Vector3 position = transform.localPosition;
		position.y = elevation * HexMetrics.elevationStep;
		position.y += (HexMetrics.SampleNoice(position).y * 2f - 1f) * HexMetrics.elevationPerturbStrength; // Add pertubation to cells vertical posititon
		transform.localPosition = position;

		Vector3 uiPosition = uiRect.localPosition;
		uiPosition.z = -position.y;
		uiRect.localPosition = uiPosition;
	}

	void Refresh () {
		if (chunk) { // Only refresh when chunk is set
			chunk.Refresh();
			for  (int i = 0; i < neighbors.Length; i++) {
				HexCell neighbor = neighbors[i];
				// Refresh neighboring chunks
				if (neighbor != null && neighbor.chunk != chunk) {
					neighbor.chunk.Refresh();
				}
			}
		}

		RemoveProps();
		AddProps();
	}

	void AddProps () {
		
		if (terrainTypeIndex == (int)HexType.FOREST) {
			Forest forest = Instantiate(propManager.GetForest(level));
			forest.transform.localPosition = this.Position; // Set position
			forest.transform.SetParent(this.transform, true); // Set parent
		}

		if (terrainTypeIndex == (int)HexType.ROCK) {
			Rock rock = Instantiate(propManager.GetRock(level));
			rock.transform.localPosition = this.Position;
			rock.transform.SetParent(this.transform, true);
		}
	}

	void RemoveProps () {
		if (this.transform.childCount > 0) {
			GameObject forest = this.transform.GetChild(0).gameObject;
			if (forest != null) {
				Destroy(forest);
			}

			GameObject rock = this.transform.GetChild(0).gameObject;
			if (rock != null) {
				Destroy(rock);
			}
		}
		
	}

	public int TerrainTypeIndex {
		get {
			return terrainTypeIndex;
		}
		set {
			if (terrainTypeIndex != value) {
				terrainTypeIndex = value;

				Refresh();
			}
		}
	}

	public int WaterLevel {
		get {
			return waterLevel;
		}
		set {
			if (waterLevel == value) {
				return;
			}
			waterLevel = value;
			Refresh();
		}
	}

	public bool IsUnderWater{
		get {
			return waterLevel > elevation;
		}
	}

	public float WaterSurfaceY {
		get {
			return (waterLevel + HexMetrics.waterElevationOffset) * HexMetrics.elevationStep;
		}
	}

	public void Save (BinaryWriter writer) {
		writer.Write((byte)terrainTypeIndex); // Use bytes to save space. Byte can be value between 0 and 255
		writer.Write((byte)elevation);
		writer.Write((byte)waterLevel);
	}

	public void Load (BinaryReader reader) {
		terrainTypeIndex = reader.ReadByte();
		elevation = reader.ReadByte();
		RefreshPosition(); // Refresh elevation
		waterLevel = reader.ReadByte();
	}
}
