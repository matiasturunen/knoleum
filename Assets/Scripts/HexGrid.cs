﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class HexGrid : MonoBehaviour {

	public int cellCountX = 20;
	public int cellCountZ = 15;

	public HexCell cellPrefab;
	public Text cellLabelPrefab;

	public Texture2D noiseSource;

	public HexGridChunk chunkPrefab;

	HexCell[] cells;

	int chunkCountX;
	int chunkCountZ;

	HexGridChunk[] chunks;

	void Awake () {
		HexMetrics.noiseSource = noiseSource;

		CreateMap(cellCountX, cellCountZ);
	}

	public bool CreateMap (int x, int z) {
		// Check if given map size is possible to do
		if (
			x <= 0 || x % HexMetrics.chunkSizeX != 0 ||
			z <= 0 || z % HexMetrics.chunkSizeZ != 0 
		) {
			Debug.LogError("Unsupported map size!");
			return false;
		}

		// Destroy previous data if exists
		if (chunks != null) {
			for (int i = 0; i < chunks.Length; i++) {
				Destroy(chunks[i].gameObject);
			}
		}

		cellCountX = x;
		cellCountZ = z;

		chunkCountX = cellCountX / HexMetrics.chunkSizeX;
		chunkCountZ = cellCountZ / HexMetrics.chunkSizeZ;

		CreateChunks();
		CreateCells();

		return true;
	}

	void CreateChunks () {
		chunks = new HexGridChunk[chunkCountX * chunkCountZ];

		for (int z = 0, i = 0; z < chunkCountZ; z++) {
		 	for (int x = 0; x < chunkCountX; x++) {
				HexGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
				chunk.transform.SetParent(transform);
			}
		}
	}

	void CreateCells () {
		cells = new HexCell[cellCountX * cellCountZ];

		for (int z = 0, i = 0; z < cellCountZ; z++) {
			for (int x = 0; x < cellCountX; x++) {
				CreateCell(x, z, i++);
			}
		}
	}

	void CreateCell(int x, int z, int i) {
		Vector3 position;
		position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
		position.y = 0f;
		position.z = z * (HexMetrics.outerRadius * 1.5f);

		HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
		//cell.transform.SetParent(transform, false);
		cell.transform.localPosition = position;
		cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);

		if (x > 0) {
			cell.SetNeighbor(HexDirection.W, cells[i - 1]); // Set west neighbor of this cell. Also cretes east neigbor for opposite cell
		}
		if (z > 0) {
			if ((z & 1) == 0) { // z & 1 is binary opration. Checks if z is even. So this adds neighbors for even rows
				cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX]);
				if (x > 0) {
					cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX - 1]);
				}
			} else { // Add neighbors to odd rows
				cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX]);
				if (x < cellCountX - 1) {
					cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX + 1]);
				}
			}
		}

		Text label = Instantiate<Text>(cellLabelPrefab);
		//label.rectTransform.SetParent(gridCanvas.transform, false);
		label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
		label.text = cell.coordinates.ToStringOnSeparateLines();

		cell.uiRect = label.rectTransform;

		cell.Elevation = 0;

		AddCellToChunk(x, z, cell);
	}

	public HexCell GetCell (Vector3 position) {
		position = transform.InverseTransformPoint(position);
		HexCoordinates coordinates = HexCoordinates.FromPosition(position);
		int index = coordinates.X + coordinates.Z * cellCountX + coordinates.Z / 2;
		return cells[index];
	}

	void OnEnable() {
		if (!HexMetrics.noiseSource) {
			HexMetrics.noiseSource = noiseSource;
		}

	}

	void AddCellToChunk(int x, int z, HexCell cell) {
		int chunkX = x / HexMetrics.chunkSizeX;
		int chunkZ = z / HexMetrics.chunkSizeZ;
		HexGridChunk chunk  = chunks[chunkX + chunkZ * chunkCountX];

		int localX = x - chunkX * HexMetrics.chunkSizeX;
		int localZ = z - chunkZ * HexMetrics.chunkSizeZ;
		chunk.AddCell(localX + localZ * HexMetrics.chunkSizeX, cell);
	}

	public HexCell GetCell(HexCoordinates coordinates) {
		int z = coordinates.Z;
		if (z < 0 || z >= cellCountZ) {
			return null;
		}

		int x = coordinates.X + z / 2;
		if (x < 0 || x >= cellCountX) {
			return null;
		}

		return cells[x + z * cellCountX];
	}

	public void ShowUI(bool visible) {
		for (int i = 0; i < chunks.Length; i++) {
			chunks[i].ShowUI(visible);
		}
	}

	public void Save (BinaryWriter writer) {
		// Save map size
		writer.Write(cellCountX);
		writer.Write(cellCountZ);

		// Iterate through cells to save them
		for  (int i = 0; i < cells.Length; i++) {
			cells[i].Save(writer);
		}
	}

	public void Load (BinaryReader reader, int version) {
		// Read map size and create map of that size
		int x = reader.ReadInt32();
		int z = reader.ReadInt32();
		
		if (!CreateMap(x, z)) {
			// Map cration failed
			return;
		}

		// Iterate through cells to load them
		for (int i = 0; i < cells.Length; i++) {
			cells[i].Load(reader);
		}

		// Refresh all chunks
		for (int i = 0; i < chunks.Length; i++) {
			chunks[i].Refresh();
		}
	}
}
