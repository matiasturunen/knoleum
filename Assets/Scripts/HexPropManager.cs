﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexPropManager : MonoBehaviour {

	public Forest[] forests;
	public Rock[] rocks;

	public Forest GetForest (int level) {
		if (level <= forests.Length) {
			return forests[level];
		}
		return null;
	}

	public Rock GetRock (int level) {
		if (level <= rocks.Length) {
			return rocks[level];
		}
		return null;
	}
}
